# SICOM 3A and Master SIGMA program: Statistical/Machine learning course

## News

<!--`Lab3 (originally scheduled for Monday, October 3) **is postponed to a later date**. More info to come (check also ADE and mail)`-->

<!-- `Due to last minute constraints, the ML lab of this Monday 3 October will be done exceptionally by zoom (identifiers sent by mail)`-->

<!--
For *doctoral or erasmus students*: for the lab sessions, please come to the IMMAC sessions (group `5PMSAST6_2021_S9_BE_G2` with [ADE](https://edt.grenoble-inp.fr/2021-2022/exterieur/))

Due to the time schedule bug on Monday, October 18: for the students that miss Lab6 on Clustering (mostly IMMAC), this  is **rescheduled on Wednesday 27th, 13:30** (see ADE).-->


<!--
## `News`
- For the interested students, we can find [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/notebooks/X_deep_learning) two demo/tutorial notebooks on deep learning for image classification (convolutional neural nets) with the tensorflow 2.X platform and Keras API.
- For students who have to stay at home for health reasons, and *only those who can't attend the face-to-face course*, there is a zoom link (see the [chamilo page](https://chamilo.grenoble-inp.fr/courses/PHELMA5PMSAST6/index.php?) of the course) to participate in
videoconference to the class every monday from 15:45 to 17:45.

-->

For *doctoral or erasmus students*: for the lab sessions, please come to the EEH sessions (group `5PMSAST6_2024_S9_BE_G1` with [ADE](https://edt.grenoble-inp.fr/2024-2025/exterieur/))


<!--
##### Lab7 instructions (EEH: Tuesday, October 22- IMMAC: Friday, October 25)
- lab7  statement on clustering is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/labs/lab7_statement.md)
- Upload **at the end of the session** your lab 7 *short report* in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=123903&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; code, figures or graphics are not required)

##### Homework for **Tuesday, October 22 (EEH) or Friday, October 25 (IMMAC)**
- **read the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/slides/8_trees_randomForest_boosting.pdf)) on trees and random forests (slides 1 to 27). Slides on sequential learning and boosting are optional.
- **prepare your questions** for the course/lab session!
-->
##### Lab7 instructions
- Lab7 statement on Classification Trees is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/labs/lab7_statement.md )
- upload **at the end of the session** your lab 7 *short report* in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=123902) (pdf file from your editor, or scanned pdf file of a handwritten paper; code, figures or graphics are not required)

##### Homework for **Wednesday, October 22** (EEH) or **Friday, October 25**  (IMMAC)
- **read the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/slides/8_trees_randomForest_boosting.pdf)) on trees and random forests (slides 1 to 27). Slides on sequential learning and boosting are optional.
- **prepare your questions** for the course/lab session!

##### Lab6 instructions (IMMAC: Wednesday, October 16 - EEH: Friday, October 18)
- Lab6 statement on clustering is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/labs/lab6_statement.md )
- upload **at the end of the session** your lab 6 *short report* in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=123902) (pdf file from your editor, or scanned pdf file of a handwritten paper; code, figures or graphics are not required)

##### Homework for **Wednesday, October 16** (IMMAC) or **Friday, October 18**  (EEH)
- **read the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/slides/7_clustering.pdf)) on clustering (unsupervised classification): **read up to kernel K-means on slide 34**
- Test your understanding with this quiz on clustering: visit [gosocrative.com](https://gosocrative.com) and enter room name `MLHW` (make sure to log out before)
- **prepare your questions** for the course/lab session!

##### ~~Lab5 instructions (Monday, October 14)~~
- ~~Lab5 statement is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/labs/lab5_statement.md)~~
- ~~upload **at the end of the session** your lab 5 *short report* in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=123901) (pdf file from your editor, or scanned pdf file of a handwritten paper; code, figures or graphics are not required)~~

##### ~~Homework for **Monday, October 14**~~
- ~~**read the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/slides/6_support_vector_machines.pdf)) on support vector machines:~~
  - ~~in first reading you can skip the slides 10 to 18 (on constrained convex optimization),~~
  - ~~the introduction to random forest (appendix, slides 43 to 47) is also optional~~
- ~~Test your understanding with this quiz on svm: visit [gosocrative.com](https://gosocrative.com) and enter room name `MLHW` (make sure to log out before)~~
- ~~**prepare your questions** for the course/lab session!~~

##### ~~Lab4 instructions~~
- ~~Lab4 statement is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/labs/lab4_statement.md)~~
- ~~upload **at the end of the session** your lab 4 *short report* in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=121243) (pdf file from your editor, or scanned pdf file of a handwritten paper; code, figures or graphics are not required)~~


##### ~~Homework for **Wednesday, October 9**~~
- **read the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/slides/5_linear_models_regularization.pdf)) on lasso and logistic regression: **read up to end of the slides**
- ~~Test your understanding with this quiz on linear models and lasso: visit [gosocrative.com](https://gosocrative.com) and enter room name `MLHW` (make sure to log out before)~~
- ~~prepare your questions for the course/lab session!~~


##### ~~Lab3 instructions~~

- ~~Lab3 statement is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/labs/lab3_statement.md)~~
- ~~upload **at the end of the session** your lab 3 *short report* in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=119340) (pdf file from your editor, or scanned pdf file of a handwritten paper; code, figures or graphics are not required)~~

##### ~~Homework for **Friday, October 4**~~

- ~~**read the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/slides/5_linear_models_regularization.pdf)) on linear models: **read up to ridge regression slide 23**~~
- ~~prepare your questions for the course/lab session!~~
- ~~Test your understanding with this quiz on linear models and ridge regression: visit [gosocrative.com](https://gosocrative.com) and enter room name `MLHW` (make sure to log out before)~~

##### ~~Lab2 instructions~~

- ~~Lab2 statement is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/labs/lab2_statement.md)~~
- ~~upload **at the end of the session** your lab 2 *short report* in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=118376) (pdf file from your editor, or scanned pdf file of a handwritten paper; code, figures or graphics are not required)~~

##### ~~Homework for **Monday, September 30**~~

- ~~**read the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/slides/4_discriminant_analysis.pdf) ) on generative models: discriminant analysis + naïve Bayes~~
- ~~prepare your questions for the course/lab session!~~
- ~~Test your understanding with this quiz on generative models: discriminant analysis + naïve Bayes: visit [gosocrative.com](https://gosocrative.com) and enter room name `MLHW` (make sure to log out before)~~


##### ~~Lab1 instructions (Wednesday, September 25)~~
- ~~Lab1 statement is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/labs/lab1_statement.md)~~
- ~~upload your lab 1 *short report* in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=117582) (pdf file from your editor, or scanned pdf file of a handwritten paper;
code, figures or graphics are not required)~~

##### ~~Homework before the first lab on **Wednesday, September 25**~~
- ~~read and run the [introduction notebooks](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/notebooks/1_introduction/) `N1_Linear_Classification.ipynb` and `N2_Polynomial_Classification_Model_Complexity.ipynb`~~
- ~~answer the questions of the notebook exercises and upload it (pdf file from your editor, or scanned pdf file of a handwritten sheet) under chamilo in the [assignment tool](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=117272) (those and only those who do not yet have an agalan account can send it to me by email):~~
  - ~~only text explanations are required, no need to copy/paste figure or graphics!~~
  - ~~must not exceed half a length of A4 paper~~


##### First course session will take place Wednesday 10:30, September 18  at Minatec Z206.

## Welcome to the Statistical Learning course!

You will find in this gitlab repository the necessary material for the teaching of _Machine Learning_:

- course materials for the lessons ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/slides))
- examples and exercises for the labs in the form of [Jupyter python notebooks](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/notebooks) (`.ipynb` files) and/or via online applications,
- quiz: online tool [Socrative](https://b.socrative.com/login/student/), room *MLSICOM*

These resources will be updated as the sessions progress.

### How to use the notebooks?

The examples and exercises will be done under python 3.x through [scikit-learn](https://scikit-learn.org/), and also [tensorflow](https://www.tensorflow.org/). These are two of the most widely used machine learning packages.

The _Jupyter Notebooks_ (`.ipynb` files) are programs containing both cells of code (for us Python) and cells of markdown text for the narrative side. These notebooks are often used to explore and analyze data. Their processing is done with a `juypyter-lab` application, which is accessed through a web browser, or directly with an IDE like vscode (suitably configured).

In order to run them you have several possibilities:

1. Download the notebooks to run them on your machine. This requires a Python environment (> 3.3), and the Jupyter and scikit-learn packages. It is recommended to install them via the [anaconda](https://www.anaconda.com/downloads) distribution which will directly install all the necessary dependencies.


**Or**

2. Use a `jupyterhub` online service:

  - we recommend the UGA's service, [gricad-jupyter.univ-grenoble-alpes.fr](https://gricad-jupyter.univ-grenoble-alpes.fr/), so that you can run your notebooks on the UGA's computation server while saving your modifications and results. Also useful to launch a background computation (connection with your Agalan account; requires uploading your notebooks+data to the server).
  - alternatively you can use an equivalent `jupyterhub` service. For example the one from google, namely [google-colab](https://colab.research.google.com/), which allows you to run/save your notebooks and also to _share the edition to several collaborators_ (requires a google account and upload your notebooks+data in your Drive)


**Or**

3. `<Deprecated>` Use the _mybinder_ service ans links to run them interactively and remotely (online): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fchatelaf%2Fml-sicom3a/master?urlpath=lab/tree/notebooks) (open the link and wait a few seconds for the environment to load).<br>
  **Warning:** Binder is meant for _ephemeral_ interactive coding, meaning that your own modifications/codes/results will be lost when your user session will automatically shut down (basically after 10 minutes of inactivity)

**Note :** You will also find among the notebooks an introduction to Python (*New*: add a focus on coding style and object-oriented programming) [notebooks/0_python_in_a_nutshell](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/notebooks%2F0_python_in_a_nutshell) 

### Miscellaneous remarks on the materials

- The slides are designed to be self-sufficient (even if the narrative side is often limited by the format).
- In addition to the slides and bibliographical/web references, we generally propose links or videos (at the beginning or end of the slides) specific to the concepts presented. These lists are of course not exhaustive, and you will find throughout the web many resources, often pedagogical. Feel free to do your own research.
- For the interested students, we can find [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/notebooks/X_deep_learning) some demo/tutorial notebooks and courses (links) on CNNs + advanced deep learning methods.


