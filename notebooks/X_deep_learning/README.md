Here are simple notebooks to introduce neural nets and CNN on a classical digit recognition task (in Keras) 

See also the following slides + notebooks on advanced deep learning methods 
(based on a course set up with Olivier Michel for ENSTA, now supervised by Nicolas Keriven)
- https://github.com/nkeriven/ensta-mt12/tree/main/slides
- https://github.com/nkeriven/ensta-mt12/tree/main/notebooks
